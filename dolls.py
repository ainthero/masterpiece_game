import pygame
from pygame import *
import os
import pyganim




class Doll1(sprite.Sprite):


    def __init__(self, hp, mag):
        sprite.Sprite.__init__(self)
        self.hp = hp // 3
        self.mp = 30
        self.max_hp = hp // 3
        self.max_mp = 30
        self.is_alive = True
        self.dfn = 1
        self.str = mag + 2
        self.agi = 1
        self.battle_image = Surface((64, 64))
        self.battle_image = pygame.image.load(os.path.join('dolls_battle', 'doll1_battle.png'))
        self.profile_image = Surface((32, 32))
        self.profile_image = pygame.image.load(os.path.join('dolls', 'doll1.png'))
        self.profile_x = -1
        self.profile_y = -1
        self.battle_x = -1
        self.battle_y = -1

    def ability(self):
        pass




class Doll2(sprite.Sprite):

    def __init__(self, hp, mag):
        sprite.Sprite.__init__(self)
        self.hp = hp // 3
        self.mp = 30
        self.max_hp = hp // 3
        self.max_mp = 30
        self.is_alive = True
        self.dfn = 1
        self.str = mag + 2
        self.agi = 1
        self.battle_image = Surface((64, 64))
        self.battle_image = pygame.image.load(os.path.join('dolls_battle', 'doll2_battle.png'))
        self.profile_image = Surface((32, 32))
        self.profile_image = pygame.image.load(os.path.join('dolls', 'doll2.png'))
        self.profile_x = -1
        self.profile_y = -1
        self.battle_x = -1
        self.battle_y = -1

    def ability(self):
        pass

class Doll3(sprite.Sprite):

    def __init__(self, hp, mag):
        sprite.Sprite.__init__(self)
        self.hp = hp // 3
        self.mp = 30
        self.max_hp = hp // 3
        self.max_mp = 30
        self.is_alive = True
        self.dfn = 1
        self.str = mag + 2
        self.agi = 1
        self.battle_image = Surface((64, 64))
        self.battle_image = pygame.image.load(os.path.join('dolls_battle', 'doll3_battle.png'))
        self.profile_image = Surface((32, 32))
        self.profile_image = pygame.image.load(os.path.join('dolls', 'doll3.png'))
        self.profile_x = -1
        self.profile_y = -1
        self.battle_x = -1
        self.battle_y = -1


    def ability(self):
        pass

class Doll4(sprite.Sprite):

    def __init__(self, hp, mag):
        sprite.Sprite.__init__(self)
        self.hp = hp // 3
        self.mp = 30
        self.max_hp = hp // 3
        self.max_mp = 30
        self.is_alive = True
        self.dfn = 1
        self.str = mag + 3
        self.agi = 1
        self.battle_image = Surface((64, 64))
        self.battle_image = pygame.image.load(os.path.join('dolls_battle', 'doll4_battle.png'))
        self.profile_image = Surface((32, 32))
        self.profile_image = pygame.image.load(os.path.join('dolls', 'doll4.png'))
        self.profile_x = -1
        self.profile_y = -1
        self.battle_x = -1
        self.battle_y = -1

    def ability(self):
        pass

class Doll5(sprite.Sprite):

    def __init__(self, hp, mag):
        sprite.Sprite.__init__(self)
        self.hp = hp // 3
        self.mp = 30
        self.max_hp = hp // 3
        self.max_mp = 30
        self.is_alive = True
        self.dfn = 1
        self.str = mag + 2
        self.agi = 1
        self.battle_image = Surface((64, 64))
        self.battle_image = pygame.image.load(os.path.join('dolls_battle', 'doll5_battle.png'))
        self.profile_image = Surface((32, 32))
        self.profile_image = pygame.image.load(os.path.join('dolls', 'doll5.png'))
        self.profile_x = -1
        self.profile_y = -1
        self.battle_x = -1
        self.battle_y = -1

    def ability(self):
        pass

class Doll6(sprite.Sprite):

    def __init__(self, hp, mag):
        sprite.Sprite.__init__(self)
        self.hp = hp // 3
        self.mp = 30
        self.max_hp = hp // 3
        self.max_mp = 30
        self.is_alive = True
        self.dfn = 1
        self.str = mag + 2
        self.agi = 1
        self.battle_image = Surface((64, 64))
        self.battle_image = pygame.image.load(os.path.join('dolls_battle', 'doll6_battle.png'))
        self.profile_image = Surface((32, 32))
        self.profile_image = pygame.image.load(os.path.join('dolls', 'doll6.png'))
        self.profile_x = -1
        self.profile_y = -1
        self.battle_x = -1
        self.battle_y = -1

    def ability(self):
        pass

class Doll7(sprite.Sprite):

    def __init__(self, hp, mag):
        sprite.Sprite.__init__(self)
        self.hp = hp // 3
        self.mp = 30
        self.max_hp = hp // 3
        self.max_mp = 30
        self.is_alive = True
        self.dfn = 1
        self.str = mag + 2
        self.agi = 1
        self.battle_image = Surface((64, 64))
        self.battle_image = pygame.image.load(os.path.join('dolls_battle', 'doll7_battle.png'))
        self.profile_image = Surface((32, 32))
        self.profile_image = pygame.image.load(os.path.join('dolls', 'doll7.png'))
        self.profile_x = -1
        self.profile_y = -1
        self.battle_x = -1
        self.battle_y = -1

    def ability(self):
        pass


class Doll8(sprite.Sprite):

    def __init__(self, hp, mag):
        sprite.Sprite.__init__(self)
        self.hp = hp // 3
        self.mp = 30
        self.max_hp = hp // 3
        self.max_mp = 30
        self.is_alive = True
        self.dfn = 1
        self.str = mag * 10
        self.agi = 1
        self.battle_image = Surface((64, 64))
        self.battle_image = pygame.image.load(os.path.join('dolls_battle', 'doll8_battle.png'))
        self.profile_image = Surface((32, 32))
        self.profile_image = pygame.image.load(os.path.join('dolls', 'doll8.png'))
        self.profile_x = -1
        self.profile_y = -1
        self.battle_x = -1
        self.battle_y = -1

    def ability(self):
        pass


class Doll9(sprite.Sprite):

    def __init__(self, hp, mag):
        sprite.Sprite.__init__(self)
        self.hp = hp // 3
        self.mp = 30
        self.max_hp = hp // 3
        self.max_mp = 30
        self.is_alive = True
        self.dfn = 1
        self.str = mag + 2
        self.agi = 1
        self.battle_image = Surface((64, 64))
        self.battle_image = pygame.image.load(os.path.join('dolls_battle', 'doll9_battle.png'))
        self.profile_image = Surface((32, 32))
        self.profile_image = pygame.image.load(os.path.join('dolls', 'doll9.png'))
        self.profile_x = -1
        self.profile_y = -1
        self.battle_x = -1
        self.battle_y = -1


    def ability(self):
        pass




