import os
import random
import pygame
import shadow
import dolls
import player
import collections
from datetime import datetime
from pygame import *
import note
import dolls



WIN_WIDTH = 800
WIN_HEIGHT = 640
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)
BACKGROUND_COLOR = "#000000"



class Battle(object):


    def __init__(self, team):
        self.team = team
        self.back = pygame.image.load(os.path.join('background', 'battle_back.png'))
        self.pick = pygame.font.Font('fonts/12081.ttf', 20).render('E', False, Color('#FFFFFF'))
        self.pick_enemy = pygame.font.Font('fonts/12081.ttf', 20).render('X', False, Color('#FFFFFF'))
        self.pick_e = pygame.image.load(os.path.join('small_details', 'pick.png'))
        self.exp = 0
        self.read = True
        self.flag = True
        self.end = False
        self.dolls_battle = []
        self.last_damage = 0
        for i in range(9):
            self.dolls_battle.append(pygame.image.load(os.path.join('dolls_battle', 'doll' + str(i + 1) + '_battle.png')))



    def main(self, a):
        bg = Surface((WIN_WIDTH, WIN_HEIGHT))
        bg.fill(Color(BACKGROUND_COLOR))
        timer = pygame.time.Clock()
        self.screen = pygame.display.set_mode(DISPLAY, FULLSCREEN)
        self.generate_enemies()
        vic = pygame.font.Font('fonts/12081.ttf', 34).render('VICTORY', False, Color('#FFFFFF'))
        self.queue = collections.deque()
        if a:
            for ch in self.team:
                self.queue.append(ch)
            for e in self.enemies:
                self.queue.append(e)
        else:
            for e in self.enemies:
                self.queue.append(e)
            for ch in self.team:
                self.queue.append(ch)
        while True:
            self.clean_buff()
            if self.team == [] or not isinstance(self.team[0], player.Player):
                self.gameover(bg)
                exit()
            if not self.team[0].is_alive:
                self.gameover(bg)
                exit()
            if self.end:
                return
            if self.is_victory(bg):
                self.plus_exp()
                self.end = True
            pygame.key.set_repeat()
            action_attack = False
            action_pass = False
            timer.tick(60)
            self.blit_interface()
            if self.queue[0] in self.enemies:
                self.shadow_ability()
                self.update_queue()
            self.update_creatures()
            if self.team == []:
                self.gameover(bg)
                return
            if self.is_victory(bg):
                self.plus_exp()
                self.end = True
            events = pygame.event.get()
            starttime = -1
            if self.read:
                for e in events:
                    if e.type == QUIT:
                        raise SystemExit
                    elif e.type == KEYDOWN and e.key == K_a and self.queue[0] == self.team[0] and self.flag:
                        starttime = pygame.time.get_ticks()
                        action_attack = True
                        self.flag = False
                        break
                    elif e.type == KEYDOWN and e.key == K_a and self.queue[0] != self.team[0] and self.queue[0] in self.team and self.flag:
                        starttime = pygame.time.get_ticks()
                        self.doll_ability()
                        self.flag = False
                        break
                    elif e.type == KEYDOWN and e.key == K_s and self.queue[0] in self.team and self.flag:
                        action_pass = True
                        starttime = pygame.time.get_ticks()
                        self.flag = False
                        self.queue[0].mp += 3
                        break
                    elif e.type == KEYDOWN and e.key == K_f and self.queue[0] == self.team[0] and self.flag:
                        starttime = pygame.time.get_ticks()
                        self.flag = False
                        if self.queue[0].mp >= 8 and len(self.team) < 3:
                            self.queue[0].mp -= 8
                            self.summon_doll()
                        break
                    elif e.type == KEYDOWN and e.key == K_d and self.queue[0] == self.team[0] and self.flag:
                        if self.queue[0].mp >= 5:
                            self.queue[0].mp -= 5
                            self.defending()
                        starttime = pygame.time.get_ticks()
                        self.flag = False
                        break
                    elif e.type == KEYDOWN and e.key == K_e and self.queue[0] in self.team and self.flag:
                        self.try_to_escape()
                        starttime = pygame.time.get_ticks()
                        self.flag = False
                        break
                    elif e.type == KEYDOWN and e.key == K_q and self.queue[0] in self.team and self.flag and self.queue[0] != self.team[0]:
                        self.queue[0].hp = 0
                        starttime = pygame.time.get_ticks()
                        self.flag = False
                        self.update_creatures()
                        break
            if self.flag == False and pygame.time.get_ticks() - starttime >= 500:
                self.flag = True
            if action_attack:
                self.attack(self.queue[0], self.choose_enemy())
                self.update_queue()
            if action_pass:
                pygame.time.wait(500)
                self.update_queue()
            action_pass = action_attack = False
            if self.queue[0] in self.team:
                self.screen.blit(self.pick, (self.queue[0].battle_x - 15, self.queue[0].battle_y + 22))
            if self.queue[0] in self.enemies:
                self.screen.blit(self.pick_enemy, (self.queue[0].battle_x + 90, self.queue[0].battle_y + 22))
            if self.is_victory(bg):
                self.plus_exp()
                self.end = True
            pygame.display.update()



    def summon_doll(self):
        self.team.append(self.choose_doll())
        self.update_queue()
        self.queue.appendleft(self.team[-1])
        self.team[0].hp -= self.team[0].hp // 3


    def choose_doll(self):
        bg = Surface((WIN_WIDTH, WIN_HEIGHT))
        bg.fill(Color(BACKGROUND_COLOR))
        timer = pygame.time.Clock()
        while True:
            timer.tick(60)
            self.screen.blit(bg, (0, 0))
            x = 200
            y = 100
            n = 0
            for i in range(3):
                y += 100
                for j in range(3):
                    n += 1
                    self.screen.blit(pygame.font.Font('fonts/12081.ttf', 34).render(str(n) + '. ', False, Color('#FFFFFF')), (x, y))
                    self.screen.blit(self.dolls_battle[n - 1], (x + 50, y))
                    x += 200
                x = 200
            for e in pygame.event.get():
                if e.type == KEYDOWN and e.key == K_1:
                    return dolls.Doll1(self.team[0].hp, self.team[0].mag)
                if e.type == KEYDOWN and e.key == K_2:
                    return dolls.Doll2(self.team[0].hp, self.team[0].mag)
                if e.type == KEYDOWN and e.key == K_3:
                    return dolls.Doll3(self.team[0].hp, self.team[0].mag)
                if e.type == KEYDOWN and e.key == K_4:
                    return dolls.Doll4(self.team[0].hp, self.team[0].mag)
                if e.type == KEYDOWN and e.key == K_5:
                    return dolls.Doll5(self.team[0].hp, self.team[0].mag)
                if e.type == KEYDOWN and e.key == K_6:
                    return dolls.Doll6(self.team[0].hp, self.team[0].mag)
                if e.type == KEYDOWN and e.key == K_7:
                    return dolls.Doll7(self.team[0].hp, self.team[0].mag)
                if e.type == KEYDOWN and e.key == K_8:
                    return dolls.Doll8(self.team[0].hp, self.team[0].mag)
                if e.type == KEYDOWN and e.key == K_9:
                    return dolls.Doll9(self.team[0].hp, self.team[0].mag)
            pygame.display.update()





    def clean_buff(self):#todo очищать str и agi
        if self.queue[0] == self.team[0] and self.queue[0].dfn == 0.3:
            self.queue[0].dfn = 1



    def choose_enemy(self):
        k = 0
        pick = self.pick_e
        timer = pygame.time.Clock()
        while True:
            timer.tick(60)
            for e in pygame.event.get():
                if e.type == QUIT:
                    raise SystemExit
                elif e.type == KEYDOWN and e.key == K_DOWN:
                    k -= 1
                    if k < 0:
                        k = len(self.enemies) - 1
                elif e.type == KEYDOWN and e.key == K_UP:
                    k += 1
                    if k >= len(self.enemies):
                        k = 0
                elif e.type == KEYDOWN and e.key == K_RIGHT:
                    k += 1
                    if k >= len(self.enemies):
                        k = 0
                elif e.type == KEYDOWN and e.key == K_LEFT:
                    k -= 1
                    if k < 0:
                        k = len(self.enemies) - 1
                elif e.type == KEYDOWN and e.key == K_a:
                    return self.enemies[k]
            if self.queue[0] in self.team:
                self.screen.blit(self.pick, (self.queue[0].battle_x - 15, self.queue[0].battle_y + 22))
            if self.queue[0] in self.enemies:
                self.screen.blit(self.pick_enemy, (self.queue[0].battle_x + 90, self.queue[0].battle_y + 22))
            self.blit_interface()
            self.screen.blit(pick, (self.enemies[k].battle_x + 8, self.enemies[k].battle_y - 16))
            pygame.display.update()


    def defending(self):
        bg = pygame.image.load(os.path.join('background', 'defend_back.png'))
        up = pygame.image.load(os.path.join('small_details', 'up.png'))
        down = pygame.image.load(os.path.join('small_details', 'down.png'))
        right = pygame.image.load(os.path.join('small_details', 'right.png'))
        left = pygame.image.load(os.path.join('small_details', 'left.png'))
        z = pygame.font.Font('fonts/12081.ttf', 32).render('Z', False, Color('#FFFFFF'))
        x = pygame.font.Font('fonts/12081.ttf', 32).render('X', False, Color('#FFFFFF'))
        timer = pygame.time.Clock()
        lst = []
        buttons = []

        for i in range(7):
            n = random.randrange(6)
            if n == 0:
                lst.append(x)
            if n == 1:
                lst.append(right)
            if n == 2:
                lst.append(left)
            if n == 3:
                lst.append(down)
            if n == 4:
                lst.append(up)
            if n == 5:
                lst.append(z)
            buttons.append(n)
        starttime = pygame.time.get_ticks()
        while True:
            timer.tick(60)
            self.screen.blit(bg, (0, 0))
            y = 410
            x = 170
            for i in range(len(lst)):
                self.screen.blit(lst[i], (x, y))
                x += 64
            for e in pygame.event.get():
                if e.type == KEYDOWN and e.key == K_x and buttons[0] == 0:
                    lst.pop(0)
                    buttons.pop(0)
                elif e.type == KEYDOWN and e.key == K_x and buttons[0] != 0:
                    self.fail()
                    return
                if e.type == KEYDOWN and e.key == K_RIGHT and buttons[0] == 1:
                    lst.pop(0)
                    buttons.pop(0)
                elif e.type == KEYDOWN and e.key == K_RIGHT and buttons[0] != 1:
                    self.fail()
                    return
                if e.type == KEYDOWN and e.key == K_LEFT and buttons[0] == 2:
                    lst.pop(0)
                    buttons.pop(0)
                elif e.type == KEYDOWN and e.key == K_LEFT and buttons[0] != 2:
                    self.fail()
                    return
                if e.type == KEYDOWN and e.key == K_DOWN and buttons[0] == 3:
                    lst.pop(0)
                    buttons.pop(0)
                elif e.type == KEYDOWN and e.key == K_DOWN and buttons[0] != 3:
                    self.fail()
                    return
                if e.type == KEYDOWN and e.key == K_UP and buttons[0] == 4:
                    lst.pop(0)
                    buttons.pop(0)
                elif e.type == KEYDOWN and e.key == K_UP and buttons[0] != 4:
                    self.fail()
                    return
                if e.type == KEYDOWN and e.key == K_z and buttons[0] == 5:
                    lst.pop(0)
                    buttons.pop(0)
                elif e.type == KEYDOWN and e.key == K_z and buttons[0] != 5:
                    self.fail()
                    return
            seconds = pygame.font.Font('fonts/12081.ttf', 128).render(str(abs(pygame.time.get_ticks() - starttime - 3000)), False, Color('#FFFFFF'))
            self.screen.blit(seconds, (200, 100))
            if pygame.time.get_ticks() - starttime >= 3000:
                self.fail()
                return
            if lst == []:
                self.queue[0].dfn = 0.3
                self.update_queue()
                return
            pygame.display.update()



    def try_attack(self):
        timer = pygame.time.Clock()
        bg = pygame.image.load(os.path.join('background', 'defend_back.png'))
        up = note.NoteUp()
        left = note.NoteLeft()
        x = note.NoteX()
        z = note.NoteZ()
        up.image = pygame.image.load(os.path.join('small_details', 'up.png'))
        left.image = pygame.image.load(os.path.join('small_details', 'left.png'))
        z.image = pygame.font.Font('fonts/12081.ttf', 32).render('Z', False, Color('#FFFFFF'))
        x.image = pygame.font.Font('fonts/12081.ttf', 32).render('X', False, Color('#FFFFFF'))
        bg = Surface((WIN_WIDTH, WIN_HEIGHT))
        bg.fill(Color(BACKGROUND_COLOR))
        lst = []
        lst_x = [250, 350, 450, 550]
        lst_symbols = [z.image, x.image, left.image, up.image]
        for i in range(15):
            n = random.randrange(4)
            if n == 0:
                lst.append(x)
            if n == 1:
                lst.append(left)
            if n == 2:
                lst.append(up)
            if n == 3:
                lst.append(z)
        start_y = -1000
        current = -1
        while True:
            timer.tick(60)
            self.screen.blit(bg, (0, 0))
            for i in range(4):
                self.screen.blit(lst_symbols[i], (lst_x[i], 600))
            pygame.draw.line(self.screen, Color('#FFFFFF'), (250, 595), (575, 595), 7)
            y_del = 0
            for i in lst:
                i.rect.y = start_y + y_del
                self.screen.blit(i.image, (i.rect.x, i.rect.y))
                y_del += 64
            if not lst:
                return 20 - len(lst)
            if lst[-1].rect.y > 620:
                self.attack_failed()
                return 20 - len(lst)
            start_y += 5
            for e in pygame.event.get():
                if e.type == KEYDOWN and e.key == K_z and lst[-1] == z and lst[-1].rect.y >= 530:
                    lst.pop(-1)
                    break
                if e.type == KEYDOWN and e.key != K_z and lst[-1] == z and lst[-1].rect.y >= 530:
                    self.attack_failed()
                    return 20 - len(lst)
                if e.type == KEYDOWN and e.key == K_x and lst[-1] == x and lst[-1].rect.y >= 530:
                    lst.pop(-1)
                    break
                if e.type == KEYDOWN and e.key != K_x and lst[-1] == x and lst[-1].rect.y >= 530:
                    self.attack_failed()
                    return 20 - len(lst)
                if e.type == KEYDOWN and e.key == K_UP and lst[-1] == up and lst[-1].rect.y >= 530:
                    lst.pop(-1)
                    break
                if e.type == KEYDOWN and e.key != K_UP and lst[-1] == up and lst[-1].rect.y >= 530:
                    self.attack_failed()
                    return 20 - len(lst)
                if e.type == KEYDOWN and e.key == K_LEFT and lst[-1] == left and lst[-1].rect.y >= 530:
                    lst.pop(-1)
                    break
                if e.type == KEYDOWN and e.key != K_LEFT and lst[-1] == left and lst[-1].rect.y >= 530:
                    self.attack_failed()
                    return 20 - len(lst)
            pygame.display.update()




    def update_queue(self):
        q = self.queue[0]
        self.queue.popleft()
        self.queue.append(q)

    def blit_interface(self):
        self.screen.blit(self.back, (0, 0))
        for r in self.enemies:
            self.screen.blit(r.image, (r.battle_x, r.battle_y))
        for ch in range(len(self.team)):
            if ch == 0:
                self.team[ch].battle_x = 80
                self.team[ch].battle_y = 120
            if ch == 1:
                self.team[ch].battle_x = 80
                self.team[ch].battle_y = 250
            if ch == 2:
                self.team[ch].battle_x = 80
                self.team[ch].battle_y = 380
            self.screen.blit(self.team[ch].battle_image, (self.team[ch].battle_x, self.team[ch].battle_y))
        self.show_hp(self.team)
        self.show_profiles(self.team)


    def doll_ability(self):
        if isinstance(self.queue[0], dolls.Doll1):
            self.queue[0].hp -= 4
            self.team[0].mp += int(10 * (1 + self.queue[0].str * 0.1))
            if self.team[0].mp > self.team[0].max_mp:
                self.team[0].mp = self.team[0].max_mp
        if isinstance(self.queue[0], dolls.Doll2):
            if self.queue[0].mp - 30 < 0:
                return
            self.queue[0].mp -= 30
            if self.team[0].str == self.team[0].str_cur:
                self.team[0].str *= 2
        if isinstance(self.queue[0], dolls.Doll3):
            if self.queue[0].mp - 10 < 0:
                return
            self.queue[0].mp -= 10
            for i in range(3):
                self.attack(self.queue[0], self.enemies[random.randrange(len(self.enemies))])
        if isinstance(self.queue[0], dolls.Doll4):
            if self.queue[0].mp - 9 < 0:
                return
            self.attack(self.queue[0], self.choose_enemy())
            self.queue[0].mp -= 9
            self.heal(self.team[0], self.last_damage)
            if self.team[0].hp > self.team[0].max_hp:
                self.team[0].hp = self.team[0].max_hp
        if isinstance(self.queue[0], dolls.Doll5):
            if self.queue[0].mp - 14 < 0:
                return
            self.queue[0].mp -= 14
            for i in self.team:
                self.heal(i, int(10 * (1 + self.queue[0].str * 0.1)))
                if i.hp > i.max_hp:
                    i.hp = i.max_hp
        if isinstance(self.queue[0], dolls.Doll6):
            if self.queue[0].mp - 30 < 0:
                return
            self.queue[0].mp -= 14
            self.team[0].agi = 100

        if isinstance(self.queue[0], dolls.Doll7):
            if self.queue[0].mp - 9 < 0:
                return
            self.queue[0].mp -= 9
            lst = self.queue
            random.shuffle(lst)
            self.queue = lst
            self.update_queue()
        if isinstance(self.queue[0], dolls.Doll8):
            if self.queue[0].mp - 11 < 0:
                return
            self.queue[0].mp -= 11
            chance = random.randrange(100)
            if chance < 50:
                self.attack(self.queue[0], self.team[random.randrange(len(self.team))])
            else:
                self.attack(self.queue[0], self.enemies[random.randrange(len(self.enemies))])
        if isinstance(self.queue[0], dolls.Doll9):
            if self.queue[0].mp - 9 < 0:
                return
            self.queue[0].mp -= 9
            chance = random.randrange(100)
            if chance < 30 and not isinstance(self.enemies[0], shadow.Demon1):
                self.enemies[random.randrange(len(self.enemies))].hp = 0
                self.update_creatures()
        self.update_queue()

    def heal(self, target, hp):
        healp = pygame.font.Font('fonts/12081.ttf', 34).render(str(hp), False, Color('#00CC00'))
        self.screen.blit(healp, (target.battle_x - 32, target.battle_y - 40))
        target.hp += hp
        pygame.display.update()
        pygame.time.delay(500)


    def shadow_ability(self):
        if isinstance(self.queue[0], shadow.ShadowWhite):
            self.attack(self.queue[0], self.team[random.randrange(len(self.team))])
        if isinstance(self.queue[0], shadow.ShadowViolet):
            self.attack(self.queue[0], self.team[random.randrange(len(self.team))])
            self.heal(self.enemies[random.randrange(len(self.enemies))], self.last_damage)
        if isinstance(self.queue[0], shadow.ShadowRed):
            for i in range(2):
                self.attack(self.queue[0], self.team[random.randrange(len(self.team))])
        if isinstance(self.queue[0], shadow.ShadowYellow):
            for i in self.team:
                self.attack(self.queue[0], i)

    def update_creatures(self):
        for r in self.enemies:
            if r.hp <= 0:
                self.kill(r)
        for r in self.team:
            if r.hp <= 0:
                self.kill(r)
        for i in range(len(self.team)):
            if not self.team[i].is_alive:
                self.team.pop(i)
                break

    def generate_enemies(self):
        enemies_amount = random.randrange(1, 4)
        self.enemies = []
        lst = [0, 1, 2, 3]
        random.shuffle(lst)
        for i in range(enemies_amount):
            f = random.randrange(1, 5)
            x = y = 0
            if lst[i] == 0:
                x = random.randrange(420, 500)
                y = 67
            elif lst[i] == 1:
                x = random.randrange(564, 600)
                y = 168
            elif lst[i] == 2:
                x = random.randrange(420, 500)
                y = 269
            elif lst[i] == 3:
                x = random.randrange(564, 600)
                y = 370
            if f == 1:
                self.enemies.append(shadow.ShadowRed(x, y))
            if f == 2:
                self.enemies.append(shadow.ShadowViolet(x, y))
            if f == 3:
                self.enemies.append(shadow.ShadowWhite(x, y))
            if f == 4:
                self.enemies.append(shadow.ShadowYellow(x, y))
        for i in self.enemies:
            i.hp += self.team[0].lvl * 5
            i.str += self.team[0].lvl
            i.agi += self.team[0].lvl




    def attack(self, by, to):
        x = 1
        self.last_damage = 0
        if by == self.team[0]:
            notes_played = self.try_attack()
            x = 1 + 0.1 * notes_played
        self.blit_interface()
        chance = random.randrange(1, 100 + to.agi * 10)
        dmg = pygame.font.Font('fonts/12081.ttf', 34).render(str(int((by.str) * to.dfn * x)), False, Color('#FFFFFF'))
        miss = pygame.font.Font('fonts/12081.ttf', 34).render('MISS', False, Color('#FFFFFF'))
        crit = pygame.font.Font('fonts/12081.ttf', 16).render('CRIT', False, Color('#FFFFFF'))
        slash = pygame.image.load(os.path.join('small_details', 'slash.png'))
        slash.set_colorkey(Color('#000000'))
        if chance > 10:
            chance = random.randrange(1, 100)
            if chance < by.agi:
                dmg = pygame.font.Font('fonts/12081.ttf', 34).render(str(int((by.str) * to.dfn * 2 * x)), False, Color('#FFFFFF'))
                to.hp -= int((by.str) * to.dfn * 2 * x)
                self.last_damage = int((by.str * to.dfn * 2 * x))
                self.screen.blit(crit, (to.battle_x - 32, to.battle_y - 40))
                self.screen.blit(dmg, (to.battle_x - 32, to.battle_y - 20))
                self.screen.blit(slash, (to.battle_x + 32, to.battle_y - 20))
            else:
                to.hp -= int((by.str * to.dfn * x))
                self.last_damage = int((by.str) * to.dfn * x)
                self.screen.blit(dmg, (to.battle_x - 32, to.battle_y - 20))
                self.screen.blit(slash, (to.battle_x + 32, to.battle_y - 20))
        else:
            self.screen.blit(miss, (to.battle_x - 32, to.battle_y - 20))
        self.show_pick()
        pygame.display.update()
        pygame.time.wait(500)


    def kill(self, who):
        for r in range(len(self.team)):
             if self.team[r] == who:
                self.team[r].hp = 0
                self.team[r].is_alive = False
        lst = []
        for r in range(len(self.enemies)):
            if self.enemies[r] == who:
                self.exp += self.enemies[r].exp_give
                self.enemies.pop(r)
                break
        for i in range(len(self.queue)):
            if self.queue[i] == who:
                del self.queue[i]
                break

    def show_hp(self, team):
        for ch in team:
            hp = pygame.font.Font('fonts/12081.ttf', 24).render('HP' + str(ch.hp) + '/' + str(ch.max_hp),
                                                                False, Color('#FFFFFF'))
            mp = pygame.font.Font('fonts/12081.ttf', 24).render('MP' + str(ch.mp) + '/' + str(ch.max_mp),
                                                                False, Color('#FFFFFF'))
            self.screen.blit(hp, (ch.profile_x + 60, ch.profile_y - 10))
            self.screen.blit(mp, (ch.profile_x + 60, ch.profile_y + 25))


    def show_profiles(self, team):
        x = 50
        for ch in team:
            ch.profile_x = x
            ch.profile_y = 560
            self.screen.blit(ch.profile_image, (ch.profile_x, ch.profile_y))
            x += 250

    def show_pick(self):
        if self.queue[0] in self.team:
            self.screen.blit(self.pick, (self.queue[0].battle_x - 15, self.queue[0].battle_y + 22))
        if self.queue[0] in self.enemies:
            self.screen.blit(self.pick_enemy, (self.queue[0].battle_x + 90, self.queue[0].battle_y + 22))


    def plus_exp(self):
        a = 0
        for i in self.team:
            if i.is_alive:
                a += 1
        n = self.exp / a
        if self.team[0].is_alive:
            self.team[0].exp += n



    def gameover(self, bg):
        over = pygame.font.Font('fonts/12081.ttf', 34).render('GAME_OVER', False, Color('#FFFFFF'))
        end = pygame.image.load(os.path.join('hero', 'end.png'))
        self.screen.blit(bg, (0, 0))
        self.screen.blit(over, (250, 320))
        self.screen.blit(end, (250, 400))
        pygame.display.update()
        pygame.time.delay(2000)
        exit()


    def fail(self):
        bg = Surface((WIN_WIDTH, WIN_HEIGHT))
        bg.fill(Color(BACKGROUND_COLOR))
        over = pygame.font.Font('fonts/12081.ttf', 34).render('defense failed', False, Color('#FFFFFF'))
        self.screen.blit(bg, (0, 0))
        self.screen.blit(over, (200, 320))
        pygame.display.update()
        pygame.time.delay(2000)
        self.update_queue()


    def attack_failed(self):
        bg = Surface((WIN_WIDTH, WIN_HEIGHT))
        bg.fill(Color(BACKGROUND_COLOR))
        over = pygame.font.Font('fonts/12081.ttf', 34).render('attack failed', False, Color('#FFFFFF'))
        self.screen.blit(bg, (0, 0))
        self.screen.blit(over, (200, 320))
        pygame.display.update()
        pygame.time.delay(1000)


    def is_victory(self, bg):
        vic = pygame.font.Font('fonts/12081.ttf', 34).render('VICTORY', False, Color('#FFFFFF'))
        if self.enemies == []:
            self.screen.blit(bg, (0, 0))
            self.screen.blit(vic, (300, 320))
            pygame.display.update()
            pygame.time.delay(2000)
            return True
        return False

    def try_to_escape(self):
        n = random.randrange(3)
        esc = pygame.font.Font('fonts/12081.ttf', 34).render('escape failed', False, Color('#FFFFFF'))
        if n == 0 or n == 1:
            self.screen.blit(esc, (self.queue[0].battle_x - 32, self.queue[0].battle_y - 20))
            self.update_queue()
            pygame.display.update()
            pygame.time.delay(1000)
            return
        self.end = True



class BossBattle(Battle):
    def shadow_ability(self):
        f = random.randrange(4)
        if f == 0:
            self.attack(self.queue[0], self.team[random.randrange(len(self.team))])
        if f == 1:
            self.attack(self.queue[0], self.team[random.randrange(len(self.team))])
            self.heal(self.enemies[random.randrange(len(self.enemies))], self.last_damage)
        if f == 2:
            for i in range(3):
                self.attack(self.queue[0], self.team[random.randrange(len(self.team))])
        if f == 3:
            for i in self.team:
                self.attack(self.queue[0], i)


    def generate_enemies(self):
        self.enemies = []
        self.enemies.append(shadow.Demon1(500, 220))