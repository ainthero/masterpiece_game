import pygame
from pygame import *
import os



PLATFORM_WIDTH = 50
PLATFORM_HEIGHT = 50
PLATFORM_COLOR = "#FFFFFF"



class PlatformWhite(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.image = Surface((PLATFORM_WIDTH, PLATFORM_HEIGHT))
        self.image = pygame.image.load(os.path.join('small_details', 'staticwhite.jpg'))
        self.rect = Rect(x, y, PLATFORM_WIDTH, PLATFORM_HEIGHT)


class PlatformRed(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.image = Surface((PLATFORM_WIDTH, PLATFORM_HEIGHT))
        self.image = pygame.image.load(os.path.join('small_details', 'staticred.jpg'))
        self.rect = Rect(x, y, PLATFORM_WIDTH, PLATFORM_HEIGHT)


class PlatformYellow(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.image = Surface((PLATFORM_WIDTH, PLATFORM_HEIGHT))
        self.image = pygame.image.load(os.path.join('small_details', 'staticyellow.jpg'))
        self.rect = Rect(x, y, PLATFORM_WIDTH, PLATFORM_HEIGHT)


class PlatformViolet(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.image = Surface((PLATFORM_WIDTH, PLATFORM_HEIGHT))
        self.image = pygame.image.load(os.path.join('small_details', 'staticviolet.jpg'))
        self.rect = Rect(x, y, PLATFORM_WIDTH, PLATFORM_HEIGHT)



class BlockBoss1(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.image = Surface((PLATFORM_WIDTH, PLATFORM_HEIGHT))
        self.image = pygame.image.load(os.path.join('small_details', 'boss.png'))
        self.rect = Rect(x, y, PLATFORM_WIDTH, PLATFORM_HEIGHT)