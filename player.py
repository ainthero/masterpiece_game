import pygame
from pygame import *
import pyganim
import os

MOVE_SPEED = 2
HEIGHT = 32
WIDTH = 32
COLOR = "#888888"
ANIMATION_DELAY = 0.1
ANIMATION_RIGHT = [('hero/heroright.png'), ('hero/heroright1.png'), ('hero/heroright2.png')]
ANIMATION_LEFT = [('hero/heroleft.png'), ('hero/heroleft1.png'), ('hero/heroleft2.png')]
ANIMATION_UP = [('hero/heroup.png'), ('hero/heroup1.png'), ('hero/heroup2.png')]
ANIMATION_DOWN = [('hero/hero.png'), ('hero/hero1.png'), ('hero/hero2.png')]


class Player(sprite.Sprite):


    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.hp = 40
        self.mp = 30
        self.max_hp = 40
        self.max_mp = 30
        self.exp = 0
        self.lvl = 1
        self.str = 2
        self.mag = 1
        self.agi = 1
        self.dfn = 1
        self.status = 0
        self.is_alive = True
        self.inv = []
        self.xvel = 0
        self.yvel = 0
        self.startX = x
        self.startY = y
        self.battle_x = 80
        self.battle_y = 120
        self.battle_image = pygame.image.load(os.path.join('hero', 'hero.png'))
        self.battle_image = pygame.transform.scale(self.battle_image, (64, 64))
        self.profile_image = pygame.image.load(os.path.join('hero', 'hero.png'))
        self.profile_image_dead = pygame.image.load(os.path.join('hero', 'hero.png'))
        self.profile_x = 50
        self.profile_y = 560
        self.agi_cur = self.agi
        self.str_cur = self.str
        self.image = Surface((WIDTH, HEIGHT))
        self.image.fill(Color(COLOR))
        self.rect = Rect(x, y, 32, 32)
        self.image.set_colorkey(Color(COLOR))
        self.direction = 0
        boltAnim = []
        for anim in ANIMATION_RIGHT:
            boltAnim.append((anim, ANIMATION_DELAY))
        self.boltAnimRight = pyganim.PygAnimation(boltAnim)
        self.boltAnimRight.play()

        boltAnim = []
        for anim in ANIMATION_LEFT:
            boltAnim.append((anim, ANIMATION_DELAY))
        self.boltAnimLeft = pyganim.PygAnimation(boltAnim)
        self.boltAnimLeft.play()

        boltAnim = []
        for anim in ANIMATION_UP:
            boltAnim.append((anim, ANIMATION_DELAY))
        self.boltAnimUp = pyganim.PygAnimation(boltAnim)
        self.boltAnimUp.play()

        boltAnim = []
        for anim in ANIMATION_DOWN:
            boltAnim.append((anim, ANIMATION_DELAY))
        self.boltAnimDown = pyganim.PygAnimation(boltAnim)
        self.boltAnimDown.play()

    def update(self, left, right, up, down, platforms):
        if left:
            self.direction = 0
            self.xvel = -MOVE_SPEED
            self.image.fill(Color(COLOR))
            self.boltAnimLeft.blit(self.image, (0, 0))
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if right:
            self.direction = 1
            self.xvel = MOVE_SPEED
            self.image.fill(Color(COLOR))
            self.boltAnimRight.blit(self.image, (0, 0))
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if not(left or right):
            self.xvel = 0
            self.collide(self.xvel, 0, platforms)



        if up:
            self.direction = 2
            self.yvel = -MOVE_SPEED
            self.image.fill(Color(COLOR))
            self.boltAnimUp.blit(self.image, (0, 0))
            self.rect.y += self.yvel
            self.collide(0, self.yvel, platforms)
            return

        if down:
            self.direction = 3
            self.yvel = MOVE_SPEED
            self.image.fill(Color(COLOR))
            self.boltAnimDown.blit(self.image, (0, 0))
            self.rect.y += self.yvel
            self.collide(0, self.yvel, platforms)
            return

        if not(up or down):
            self.yvel = 0
            self.collide(0, self.yvel, platforms)







    def collide(self, xvel, yvel, platforms):
        for p in platforms:
            if sprite.collide_rect(self, p):  # если пересекается

                if xvel > 0:
                    self.rect.right = p.rect.left
                    self.xvel = 0

                if xvel < 0:
                    self.rect.left = p.rect.right
                    self.xvel = 0

                if yvel > 0:
                    self.rect.bottom = p.rect.top
                    self.yvel = 0

                if yvel < 0:
                    self.rect.top = p.rect.bottom
                    self.yvel = 0

    def cam_func(self):
        if self.rect.x > 500:
            self.rect.x = 500
        if self.rect.x < 300:
            self.rect.x = 300

        if self.rect.y > 400:
            self.rect.y = 400
        if self.rect.y < 240:
            self.rect.y = 240

    def is_player_in_cam(self):
        f1 = f2 = f3 = f4 = False
        if self.rect.x > 500:
            return True
        if self.rect.x < 300:
            return True
        if self.rect.y > 400:
            return True
        if self.rect.y < 240:
            return True


    def set_level(self):
        if self.exp >= self.lvl * 40 + 40 * (self.lvl - 1):
            self.exp = self.exp % (self.lvl * 20 + 40 * (self.lvl - 1))
            self.lvl += 1
            self.max_mp += int(self.max_mp * 0.20)
            self.max_hp += int(self.max_hp * 0.20)
            if self.max_mp > 99:
                self.max_mp = 99
            if self.max_hp > 99:
                self.max_hp = 99
            self.mp = self.max_mp
            self.hp = self.max_hp
            return True



class Heart(sprite.Sprite):

    def __init__(self, x, y, direction):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(os.path.join('small_details', 'heart.png'))
        self.rect  = Rect(x, y, 16, 16)
        self.direction = direction
        self.start_x = x
        self.start_y = y
        self.is_alive = True


    def update(self):
        if self.direction == 0:
            self.rect.x -= 5
        if self.direction == 1:
            self.rect.x += 5
        if self.direction == 2:
            self.rect.y -= 5
        if self.direction == 3:
            self.rect.y += 5
        if self.rect.x > self.start_x + 100:
            self.is_alive = False
        if self.rect.x < self.start_x - 100:
            self.is_alive = False
        if self.rect.y > self.start_y + 100:
            self.is_alive = False
        if self.rect.y < self.start_y - 100:
            self.is_alive = False