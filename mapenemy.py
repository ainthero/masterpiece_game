import random
import pygame
import player
import block
import pyganim
import Battle
import dolls
import os

from pygame import *


WIN_WIDTH = 800
WIN_HEIGHT = 640
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)
BACKGROUND_COLOR = "#000000"
PLATFORM_WIDTH = 50
PLATFORM_HEIGHT = 50
PLATFORM_COLOR = "#FFFFFF"





class EnemyWhite(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.image = Surface((32, 32))
        self.image = pygame.transform.scale(pygame.image.load(os.path.join('enemies', 'demonwhite.png')), (32, 32))
        self.rect = Rect(x, y, 32, 32)
        self.direction = 2


    def change_direction(self):
        self.direction = random.randrange(4)

    def update(self, platforms):
        if self.direction == 0:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if self.direction == 1:
            self.xvel = +3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if self.direction == 2:
            self.yvel = -3
            self.rect.y += self.yvel
            self.collide(0, self.yvel, platforms)
            return
        if self.direction == 3:
            self.yvel = +3
            self.rect.y += self.yvel
            self.collide(0, self.yvel, platforms)
            return

    def collide(self, xvel, yvel, platforms):
        for p in platforms:
            if sprite.collide_rect(self, p):  # если пересекается

                if xvel > 0:
                    self.rect.right = p.rect.left
                    self.xvel = 0

                if xvel < 0:
                    self.rect.left = p.rect.right
                    self.xvel = 0

                if yvel > 0:
                    self.rect.bottom = p.rect.top
                    self.yvel = 0

                if yvel < 0:
                    self.rect.top = p.rect.bottom
                    self.yvel = 0
                self.change_direction()


class EnemyRed(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.image = Surface((32, 32))
        self.image = pygame.transform.scale(pygame.image.load(os.path.join('enemies', 'demonred.png')), (32, 32))
        self.rect = Rect(x, y, 32, 32)
        self.direction = 1

    def change_direction(self):
        self.direction = random.randrange(4)

    def update(self, platforms):
        if self.direction == 0:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if self.direction == 1:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if self.direction == 2:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if self.direction == 3:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return

    def collide(self, xvel, yvel, platforms):
        for p in platforms:
            if sprite.collide_rect(self, p):  # если пересекается

                if xvel > 0:
                    self.rect.right = p.rect.left
                    self.xvel = 0

                if xvel < 0:
                    self.rect.left = p.rect.right
                    self.xvel = 0

                if yvel > 0:
                    self.rect.bottom = p.rect.top
                    self.yvel = 0

                if yvel < 0:
                    self.rect.top = p.rect.bottom
                    self.yvel = 0

                self.change_direction()


class EnemyYellow(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.image = Surface((32, 32))
        self.image = pygame.transform.scale(pygame.image.load(os.path.join('enemies', 'demonyellow.png')), (32, 32))
        self.rect = Rect(x, y, 32, 32)
        self.direction = 1

    def change_direction(self):
        self.direction = random.randrange(4)


    def update(self, platforms):
        if self.direction == 0:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if self.direction == 1:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if self.direction == 2:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if self.direction == 3:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return


    def collide(self, xvel, yvel, platforms):
        for p in platforms:
            if sprite.collide_rect(self, p):  # если пересекается

                if xvel > 0:
                    self.rect.right = p.rect.left
                    self.xvel = 0

                if xvel < 0:
                    self.rect.left = p.rect.right
                    self.xvel = 0

                if yvel > 0:
                    self.rect.bottom = p.rect.top
                    self.yvel = 0

                if yvel < 0:
                    self.rect.top = p.rect.bottom
                    self.yvel = 0

                self.change_direction()



class EnemyViolet(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.image = Surface((32, 32))
        self.image = pygame.transform.scale(pygame.image.load(os.path.join('enemies', 'demonviolet.png')), (32, 32))
        self.rect = Rect(x, y, 32, 32)
        self.direction = 1

    def change_direction(self):
        self.direction = random.randrange(4)

    def update(self, platforms):
        if self.direction == 0:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if self.direction == 1:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if self.direction == 2:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return
        if self.direction == 3:
            self.xvel = -3
            self.rect.x += self.xvel
            self.collide(self.xvel, 0, platforms)
            return

    def collide(self, xvel, yvel, platforms):
        for p in platforms:
            if sprite.collide_rect(self, p):  # если пересекается

                if xvel > 0:
                    self.rect.right = p.rect.left
                    self.xvel = 0

                if xvel < 0:
                    self.rect.left = p.rect.right
                    self.xvel = 0

                if yvel > 0:
                    self.rect.bottom = p.rect.top
                    self.yvel = 0

                if yvel < 0:
                    self.rect.top = p.rect.bottom
                    self.yvel = 0

                self.change_direction()
