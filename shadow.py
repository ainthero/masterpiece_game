import os
import random
import pygame
from pygame import *



WIN_WIDTH = 800
WIN_HEIGHT = 640
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)
BACKGROUND_COLOR = "#000000"






class ShadowWhite(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.exp_give = 50
        self.hp = 20
        self.str = 4
        self.dfn = 1
        self.agi = 1
        self.image = Surface((64, 64))
        self.image = pygame.image.load(os.path.join('enemies', 'demonwhite.png'))
        self.battle_x = x
        self.battle_y = y



class ShadowRed(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.exp_give = 50
        self.hp = 24
        self.str = 1
        self.dfn = 1
        self.agi = 1
        self.image = pygame.image.load(os.path.join('enemies', 'demonred.png'))
        self.battle_x = x
        self.battle_y = y


class ShadowViolet(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.exp_give = 50
        self.hp = 24
        self.str = 3
        self.dfn = 1
        self.agi = 1
        self.image = pygame.image.load(os.path.join('enemies', 'demonviolet.png'))
        self.battle_x = x
        self.battle_y = y



class ShadowYellow(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.exp_give = 40
        self.hp = 24
        self.str = 1
        self.dfn = 1
        self.agi = 1
        self.image = pygame.image.load(os.path.join('enemies', 'demonyellow.png'))
        self.battle_x = x
        self.battle_y = y


class Demon1(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.exp_give = 70
        self.hp = 234
        self.str = 14
        self.dfn = 1
        self.agi = 2
        self.image = pygame.image.load(os.path.join('bosses', 'demon1.png'))
        self.image = pygame.transform.scale(self.image, (130, 200))
        self.battle_x = x
        self.battle_y = y

