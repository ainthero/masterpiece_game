import random
import pygame
import player
import block
import pyganim
import Battle
import dolls
import os
import mapenemy
import pickle

from pygame import *


WIN_WIDTH = 800
WIN_HEIGHT = 640
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)
BACKGROUND_COLOR = "#000000"
PLATFORM_WIDTH = 50
PLATFORM_HEIGHT = 50
PLATFORM_COLOR = "#FFFFFF"



def generate_lab(n, m):
    f = -1
    lst = []
    mvmnts = []
    for i in range(n):
        lst.append([0 for i in range(m)])
    for i in range(0, n, 2):
        for j in range(0, m, 2):
            f = random.randrange(2)
            if j + 2 >= m:
                f = 0#вниз
            if i + 2 >= n:
                f = 1#вправо
            if (j + 2 >= m) and (i + 2 >= n):
                continue
            if f == 1:
                lst[i][j] = 1
                lst[i][j + 1] = 1
                lst[i][j + 2] = 1
            else:
                lst[i][j] = 1
                lst[i + 1][j] = 1
                lst[i + 2][j] = 1
    '''for i in range(1, n - 1):
        for j in range(1, m - 1):
            if lst[i + 1][j] == 1 and lst[i - 1][j] == 1:
                f = random.randrange(100)
                if f > 60:
                    lst[i][j] = 1
            if lst[i][j + 1] == 1 and lst[i][j - 1] == 1:
                f = random.randrange(100)
                if f > 60:
                    lst[i][j] = 1'''
    '''for i in range(1, n - 1):
        for j in range(1, m - 1):
            if lst[i][j] == 0:
                if lst[i + 1][j] == 1 and lst[i - 1][j] == 1 and lst[i][j + 1] == 1:
                    if lst[i][j - 1] == 1 and lst[i + 1][j + 1] == 1:
                        if lst[i + 1][j - 1] == 1 and lst[i - 1][j + 1] == 1 and lst[i - 1][j - 1] == 1:
                            lst[i][j] = 1'''
    return lst


def main_screen(screen):
    track = pygame.mixer.music.load(os.path.join('music', 'dcoss.ogg'))
    pygame.mixer.music.play(10000)
    background = pygame.image.load(os.path.join('background', 'start.png'))
    title = pygame.font.Font('fonts/12081.ttf', 64).render('project', False, Color('#FFFFFF'))
    title1 = pygame.font.Font('fonts/12081.ttf', 64).render('uncertainty', False, Color('#FFFFFF'))
    press = pygame.font.Font('fonts/12081.ttf', 32).render('press a', False, Color('#FFFFFF'))
    screen.blit(background, (0, 0))
    screen.blit(title, (10, 50))
    screen.blit(title1, (200, 100))
    screen.blit(press, (300, 400))
    while 1:
        for e in pygame.event.get():
            if e.type == KEYDOWN and e.key == K_ESCAPE:
                raise SystemExit("QUIT")
            if e.type == pygame.QUIT:
                sys.exit()
            if e.type == KEYDOWN and e.key == K_a:
                return
        pygame.display.update()





def main():
    M, N, bg, down, entities, hero, left, lst,\
    platforms, right, screen, team, timer, up, enemies = initialize_game()
    pygame.mouse.set_visible(False)
    main_screen(screen)
    back_stats = pygame.image.load(os.path.join('background', 'map_stats.png'))
    boss_x, boss_y = initialize_field(M, N, entities, lst, platforms)
    boss = block.BlockBoss1(boss_x, boss_y)
    entities.add(boss)
    right = True
    hero.update(left, right, up, down, platforms)
    right = False
    f = False
    while True:
        timer.tick(60)
        screen.blit(bg, (0, 0))
        for e in pygame.event.get():
            if e.type == QUIT:
                raise SystemExit
            if e.type == KEYDOWN and e.key == K_LEFT and up == False and down == False:
                left = True
                break
            if e.type == KEYDOWN and e.key == K_RIGHT  and up == False and down == False:
                right = True
                break
            if e.type == KEYUP and e.key == K_RIGHT:
                right = False
                break
            if e.type == KEYUP and e.key == K_LEFT:
                left = False
                break
            if e.type == KEYDOWN and e.key == K_UP and right == False and left == False:
                up = True
                break
            if e.type == KEYDOWN and e.key == K_DOWN  and right == False and left == False:
                down = True
                break
            if e.type == KEYUP and e.key == K_UP:
                up = False
                break
            if e.type == KEYUP and e.key == K_DOWN:
                down = False
                break
            if e.type == KEYDOWN and e.key == K_a and not f:
                heart = player.Heart(hero.rect.x + 8, hero.rect.y + 8, hero.direction)
                entities.add(heart)
                f = True
        hero.cam_func()
        if f:
            heart.update()
            if not heart.is_alive:
                entities.remove(heart)
                del heart
                f = False
        for enemy in enemies:
            enemy.update(platforms)
            if sprite.collide_rect(hero, enemy):
                enemies.remove(enemy)
                right = left = up = down = False
                make_battle(team, screen, False)
                hero.str = hero.str_cur
                hero.agi = hero.agi_cur
            if f and sprite.collide_rect(heart, enemy):
                enemies.remove(enemy)
                make_battle(team, screen, True)
                hero.str = hero.str_cur
                hero.agi = hero.agi_cur
        if sprite.collide_rect(hero, boss):
            btl = Battle.BossBattle(team)
            btl.main(True)
            end(bg, screen)
        hero.update(left, right, up, down, platforms)
        scrolling_map(entities, hero, enemies)
        entities.draw(screen)
        enemies.draw(screen)
        screen.blit(back_stats, (0, 0))
        blit_interface(team, hero, screen)
        pygame.display.update()


def make_battle(team, screen, a):
    btl = Battle.Battle(team)
    btl.main(a)
    if team == []:
        exit()
    team[0].str = team[0].str_cur
    team[0].agi = team[0].agi_cur
    f = team[0].set_level()
    if f:
        n = 4
        bg = Surface((WIN_WIDTH, WIN_HEIGHT))
        bg.fill(Color(BACKGROUND_COLOR))
        while n != 0:
            scores = pygame.font.Font('fonts/12081.ttf', 32).render(str(n) + '/4', False, Color('#FFFFFF'))
            screen.blit(bg, (0, 0))
            levelup = pygame.font.Font('fonts/12081.ttf', 32).render("LEVEL UP", False, Color('#FFFFFF'))
            screen.blit(scores, (390, 150))
            screen.blit(levelup, (350, 120))
            srt = pygame.font.Font('fonts/12081.ttf', 32).render("1.   STR" + "  " + str(team[0].str), False, Color('#FFFFFF'))
            mag = pygame.font.Font('fonts/12081.ttf', 32).render("2.   MAG" + "  " + str(team[0].mag), False, Color('#FFFFFF'))
            agi = pygame.font.Font('fonts/12081.ttf', 32).render("3.   AGI" + "  " + str(team[0].agi), False, Color('#FFFFFF'))
            warning = pygame.font.Font('fonts/12081.ttf', 16).render("press numbers which correspond parameters you want upgrade", False, Color('#FFFFFF'))
            cannot = pygame.font.Font('fonts/12081.ttf', 16).render("you can't cancel upgrade after press number", False, Color('#FFFFFF'))
            screen.blit(srt, (250, 200))
            screen.blit(mag, (250, 250))
            screen.blit(agi, (250, 300))
            screen.blit(warning, (10, 590))
            screen.blit(cannot, (10, 610))
            pygame.display.update()
            for e in pygame.event.get():
                if e.type == KEYDOWN and e.key == K_1:
                    n -= 1
                    team[0].str += 1
                    team[0].str_cur += 1
                    if team[0].str > 50:
                        team[0].str = 50
                if e.type == KEYDOWN and e.key == K_2:
                    n -= 1
                    team[0].mag += 1
                    if team[0].mag > 50:
                        team[0].mag = 50
                if e.type == KEYDOWN and e.key == K_3:
                    n -= 1
                    team[0].agi += 1
                    team[0].agi_cur += 1
                    if team[0].agi > 50:
                        team[0].agi = 50



def initialize_game():
    M = 50
    N = 50
    pygame.init()
    screen = pygame.display.set_mode(DISPLAY, FULLSCREEN)
    pygame.display.set_caption("project uncertainty")
    bg = Surface((WIN_WIDTH, WIN_HEIGHT))
    bg.fill(Color(BACKGROUND_COLOR))
    lst = generate_lab(M, N)
    entities = pygame.sprite.Group()  # все объекты
    enemies = pygame.sprite.Group()
    platforms = []  # коллайды
    timer = pygame.time.Clock()
    hero = player.Player(300, 400)
    team = [hero]
    n = 8
    for i in range(1, len(lst) - 1):
        for j in range(1, len(lst[i]) - 1):
            if lst[i][j] == 1 and lst[i][j - 1] == 1 and lst[i][j + 1] == 1:
                f = random.randrange(100)
                if f > 96 and n > 0:
                    enemies.add(mapenemy.EnemyWhite(250 + PLATFORM_HEIGHT * j, 350 + PLATFORM_WIDTH * i))
                    n -= 1
    entities.add(hero)
    left = right = up = down = False
    return M, N, bg, down, entities, hero, left, lst, platforms, right, screen, team, timer, up, enemies



def scrolling_map(entities, hero, enemies):
    for e in entities:
        if e != hero and hero.is_player_in_cam():
            e.rect.x -= hero.xvel
            e.rect.y -= hero.yvel
    for e in enemies:
        if e != hero and hero.is_player_in_cam():
            e.rect.x -= hero.xvel
            e.rect.y -= hero.yvel

def initialize_field(M, N, entities, lst, platforms):
    x = 250
    y = 350
    boss_x = 0
    boss_y = 0
    for i in range(len(lst)):
        for j in range(len(lst[i])):
            if i == 0 or j == 0 or i == len(lst) - 1 or j == len(lst[i]) - 1:
                lst[i][j] = 0
    for i in range(1, 4):
        for j in range(1, 4):
            lst[i][j] = 1
        for j in range(len(lst[i]) - 4, len(lst[i]) - 1):
            lst[i][j] = 1
    for i in range(len(lst) - 4, len(lst) - 1):
        for j in range(1, 4):
            lst[i][j] = 1
        for j in range(len(lst[i]) - 4, len(lst[i]) - 1):
            lst[i][j] = 1
    lst[len(lst) - 2][len(lst[0]) - 2] = 2
    for row in lst:
        for i in row:
            if i == 0:
                pf = block.PlatformWhite(x, y)
                entities.add(pf)
                platforms.append(pf)
            if i == 2:
                boss_x = x
                boss_y = y
            x += PLATFORM_HEIGHT
        y += PLATFORM_WIDTH
        x = 250
    """x = 250
    y = 350
    for row in range(N + 2):
        for i in range(M + 2):
            if i == 2 or i == N + 1 or row == 0 or row == M - 1:
                pf = block.PlatformWhite(x, y)
                entities.add(pf)
                platforms.append(pf)
            x += PLATFORM_HEIGHT
        y += PLATFORM_WIDTH
        x = 150"""
    return boss_x, boss_y



def blit_interface(team, hero, screen):
    hp = pygame.font.Font('fonts/12081.ttf', 24).render('PARTY:', False, Color('#FFFFFF'))
    screen.blit(hp, (15, 15))
    x = 125
    for i in team:
        screen.blit(i.profile_image, (x, 15))
        x += 50
    leader = pygame.font.Font('fonts/12081.ttf', 24).render("YUKI'S HP", False, Color('#FFFFFF'))
    screen.blit(leader, (290, 15))
    pygame.draw.line(screen, Color('#FFFFFF'), (498, 32), (702, 32), 10)
    pygame.draw.line(screen, Color('#000000'), (500, 32), (500 + 200 * (hero.hp / hero.max_hp), 32), 6)



def end(bg, screen):
    demons = pygame.font.Font('fonts/12081.ttf', 34).render('YOU CAN ADVANCE', False, Color('#FFFFFF'))
    screen.blit(bg, (0, 0))
    end = pygame.image.load(os.path.join('hero', 'end_good.png'))
    screen.blit(demons, (150, 320))
    screen.blit(end, (250, 400))
    pygame.display.update()
    pygame.time.delay(3000)
    exit()

if __name__ == '__main__':
    main()